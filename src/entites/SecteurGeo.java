package entites;
import java.util.LinkedList;
import java.util.List;

public class SecteurGeo {

    private String         codeSecteur;
    private String         libSecteur;
    
    private List<Salarie>  lesSalaries=new LinkedList();
    
    public Float sommeSalaires(){
        
        Float sommeSal=0.0F;
        for(Salarie sal : lesSalaries){
            sommeSal +=sal.salaireBrut();
        }
        
        return sommeSal;
        
    }
     
    //<editor-fold defaultstate="collapsed" desc="gets & sets">
    
    public String getCodeSecteur() {
        return codeSecteur;
    }
    
    public String getLibSecteur() {
        return libSecteur;
    }
    
    public void setCodeSecteur(String codeSecteur) {
        this.codeSecteur = codeSecteur;
    }
    
    public void setLibSecteur(String libSecteur) {
        this.libSecteur = libSecteur;
    }
    
    public List<Salarie> getLesSalaries() {
        return lesSalaries;
    }

    public void setLesSalaries(List<Salarie> lesSalaries) {
        this.lesSalaries = lesSalaries;
    }
    //</editor-fold>   
}
