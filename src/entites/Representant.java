
package entites;

import java.util.LinkedList;
import java.util.List;

public class Representant extends Salarie{

    private Float tauxComRep;
    private Float plafondFraisDep;
    private String matricule;
    private String nomprenom;
    private Float commision;
    private Float ca, salaireDeBase, salaireBrutR;
    private Float totalFraisDep;
    private Float salaireBrut;

    private List<Vente> lesVentes=new LinkedList();
    
      public Float ca (){
        Float somme=0F;
        for(Vente v:lesVentes){
            somme+=v.getMontantFact();
        }
        return somme;
    }
    
        public Float commision(){
        Float com=0F;
        if(ca()>=20000){
            com+=600+(this.ca()-20000)*0.06F;
            
        }
        else{
            if(this.ca()>10000){
                com=200+(this.ca()-10000)*0.04F;
            }
            if(this.ca()<10000){
                com=this.ca()*0.02F;
            }
           
        }
        return com;
        
    }
    
        public Float totalFraisDep(){
            float somme=0.0F;
            for(Vente v : lesVentes){
                somme+=v.getFraisDep();
                
            }
            return somme;
        }
        public Float salaireBrutR(){
        salaireBrutR = salaireDeBase+commision+totalFraisDep;
        return salaireBrutR;
                }
    @Override
     public void afficher(){
        System.out.println(matricule+" "+nomprenom+" "+salaireBrut+" "+commision+" "+ca+" "+totalFraisDep+" "+salaireBrutR);
    }
   
    //<editor-fold defaultstate="collapsed" desc="gets & sets">
    
    public Float getTauxComRep() {
        return tauxComRep;
    }
    
    public Float getPlafondFraisDep() {
        return plafondFraisDep;
    }
    
    public void setTauxComRep(Float tauxComRep) {
        this.tauxComRep = tauxComRep;
    }
    
    public void setPlafondFraisDep(Float plafondFraisDep) {
        this.plafondFraisDep = plafondFraisDep;
    }
    
     public List<Vente> getLesVentes() {
        return lesVentes;
    }

    public void setLesVentes(List<Vente> lesVentes) {
        this.lesVentes = lesVentes;
    }
    //</editor-fold>   
}
