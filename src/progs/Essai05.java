
package progs;

import entites.Representant;
import entites.Salarie;
import entites.Vente;

public class Essai05 {

    public static void main(String[] args) {
        
        Salarie s = data.Dao.getLeSalarie("R002");
    
        System.out.println(s.getNomprenom()+ " Salaire de base: "+s.getSalaireDeBase()+ " €");
        
        System.out.println();
        //vérifie que s est de type representant
        if ( s instanceof Representant){
            //r est un representant en forçant (cast) le type de s à reprensentant
            Representant r = (Representant)s;  
            
            for (Vente v : r.getLesVentes()){
            
                System.out.printf("%6d %8.2f €\n",v.getNumFact(),v.getMontantFact());
            }
            System.out.println();
        }
        
    }
}
