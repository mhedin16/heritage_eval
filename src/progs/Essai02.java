
package progs;

import entites.Salarie;
import entites.SecteurGeo;

public class Essai02 {

    public static void main(String[] args) {
        SecteurGeo sg =data.Dao.getLeSecteurGeo("NPDC");
        
        if (sg != null) {
            
           System.out.println(sg.getLibSecteur());
           System.out.println();
           
           for (Salarie sal : sg.getLesSalaries()){
           
               System.out.println(sal.getNomprenom());   
           }
        }
        
        System.out.println();  
    }
}
